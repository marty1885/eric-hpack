### eric-hpack
遵守 std c++11 的 hpack 實作

#### Build
##### Build unittest
``` bash
g++ --std=c++11 -O2 -o unittest unittest.cpp
```
> 會 include hpack.cpp, 導入所有函式及內部變數, 沒有 include guard
##### Build Benchmark
```bash
g++ --std=c++11 -O2 -o benchmark benchmark.cpp hpack.cpp
```
> 正常使用 eric-hpack 的方式

所有的實作都在 hpack.cpp, hpack.h 只有要公開的 class 宣告
建議複製  hpack.cpp, hpack.h 到專案文件夾中直接使用
##### Config
###### 旗標
UNITTEST
> 會產生 main 函數並執行單元測試，需要 DEBUGFLAG

DEBUGFLAG
> 會編譯 debug 用的函式及加入檢查狀態用的判斷式，會引響執行速度，狀態異常時用 printf 顯示
##### 常數
- `bool DecodeStringLiteralOctet;` 
> 是否要解碼 Octet String Literal
- `bool HpackDecoderCheckParameter`
> 是否要檢查 Hpack::Decoder 的參數是否合理 
- `bool LiteralDecodeCheckLiteralPad`
> 是否要檢查 LiteralDecode 到字串結尾的狀態
- `bool HpackDecoderTableUpdateHeaderBlank`
> 當 Decoder 收到 DynamicTableSizeUpdate 時，HpackDecoderTableUpdateHeaderBlank 為假時會移除空的 header 行，在沒有 `std::vector<HeaderFieldInfo> &hfdilst` 的版本，有 hfdilst 版本API 則不會移除，應為要在帶有 `std::vector<HeaderFieldInfo> &hfdilst` 的版本 API 時使 `std::vector<KeyValPair> &header` 與 hfdilst Vector 長度一致，在無 `std::vector<HeaderFieldInfo> &hfdilst` 版本時也會留下空行。
> 此選項為假時會增加運行耗時
- `Size_t CircularBufferIncreaseNumber`
> IndexTable::dynamicTable 擴容時每次增加的大小 
#### Hpack
所有 API 都有 `HpRBuffer &stream` 和 `std::shared_ptr<HpRBuffer> stream` 兩種版本
```cpp
HpackStatus Decoder(HpRBuffer &stream, std::vector<KeyValPair> &header);
HpackStatus Decoder(std::unique_ptr<HpRBuffer> stream, std::vector<KeyValPair> &header);
```
stream 供讀取的 HpRBuffer，如果 stream 長度為零時，HpackDecoderCheckParameter 為真時會返 HpackStatus::RBuffZeroLength，為假時為未定義行為
```cpp
HpackStatus Encoder(HpWBuffer &stream,const std::vector<KeyValPair> &header);
HpackStatus Encoder(std::unique_ptr<HpWBuffer> stream,const std::vector<KeyValPair> &header);
```
由於 HpWBuffer 異常和超過限制大小都會回傳 WBuffErr* 系列的錯誤，所以讀到WBuffErr* 系列錯誤時需要先檢查 HpWBuffer 的狀態，如果用 `MakeHpWBuffer(std::vector<unsigned char> &ss)` 沒有異常狀態只會滿
```cpp
Encoder(HpWBuffer &stream,const std::vector<KeyValPair> &header, std::vector<HeaderFieldInfo> &hfdilst);
HpackStatus Decoder(HpRBuffer &stream, std::vector<KeyValPair> &header, std::vector<HeaderFieldInfo> &hfdilst);
```
參數中有 `std::vector<HeaderFieldInfo>` 用以表明讀到/要寫入的 Header Field Representation,
Encoder 時
`hfdilst.size() == header.size()` 必須為真，應為 hfdilst 和 header 必須一一對應指定，否則會回傳 HfdiListLength 錯誤
hfdilst 只能填入
- RLiteralHeaderFieldNeverIndexed
- RLiteralHeaderFieldWithoutIndexing
- RLiteralHeaderFieldAlwaysIndex
- RLiteralHeaderFieldWithIncrementalIndexing

不能填入 RIndexedHeaderField，應為 RIndexedHeaderField, RLiteralHeaderFieldWithIncrementalIndexing 是由是否在動態表中找到決定的，但還是可以強制增加條目
```cpp
HpackStatus Encoder(HpWBuffer &stream,const std::vector<KeyValPair> &header, const std::vector<std::string> &exclude);
```
exclude 表明如果 header 的  key 在其中的話編碼為 RLiteralHeaderFieldNeverIndexed，區分大小寫，原則上 Http2 的 header key 為全小写
```cpp
HpackStatus Encoder(HpWBuffer &stream,const std::vector<KeyValPair> &header, const StringLiteralEncodeWay SLEW)const;
```
有 `StringLiteralEncodeWay SLEW` 參數的系列函數用已表明 Encoder 要用哪一種方式編碼 String Literal，編碼選項如下表
```cpp
enum StringLiteralEncodeWay {
    EOctet,
    EHuffman,
    EShortest
};
```
> EShortest 計算 EOctet 和 EHuffman 編碼的長度，選最短的，用以解決 Huffman 編碼有時比不編碼還長的問題，也是其他沒有 `StringLiteralEncodeWay SLEW` 參數的 Encode 函式的預設
#### HpRBuffer
```c
class HpRBuffer {
    // 0 length not allowed
  public:
    virtual ~HpRBuffer() = default;
    virtual bool Next() =0;
    virtual size_t Size() = 0;
    virtual unsigned char Current() = 0;
};
```

歡迎實施自己的 HpRBuffer ，HpRBuffer 在 decode 流程中使用
Next() 在異常及結尾時皆回傳 False，必須擔保 Next() 在回傳 False 之後再呼叫必須一直回傳False
Currrent() 必須在建構之後到 Next() 回傳 False 前可用，所以不可以出現空的 buffer，Next() 回傳 False 之後可以回傳任意值
不用擔保 HpRBuffer 是否反映在建構式呼叫到第一次任意函式呼叫間的底層狀態反映
VecHpRBuffer 底層改動會反映，ArrHpRBuffer 長度不會值會
#### HpWBuffer
```cpp
class HpWBuffer {
  public:
    ~HpWBuffer() = default;
    virtual size_t Size() = 0;
    virtual bool Append(unsigned char i) = 0;
    virtual unsigned char OrCurrent(unsigned char i) = 0;
};
```
歡迎實施自己的 HpWBuffer ，HpWBuffer 在 encode 流程中使用

`Hpack::MakeHpWBuffer` 預設寫入上限是 8192, benchmark.cpp 會不夠用，需要用 `Hpack::MakeHpWBuffer(pbuff, 10240)`  顯式傳入寫入上限
#### Type Size
```c
typedef int_fast16_t Integer2byte;
typedef uint_fast64_t Uinteger5byte;
```
- `enum` 用預設的 `int`
- `size_t` large then 16 bit，在程式中表達 IndexTable 及 Buffer 等的最大限制
- `Integer2byte`  large then 16 bit,在程式中表達 sym, 需要自少可以表示 -1
- `unsigned char` Must 8 bit
- `Uinteger5byte` large then 46 bit, multiples of `unsigned char`
Header Field Representation 中字串長度及 Indexing Tables 的 index 在邊解碼器中混用 size_t 和 Uinteger5byt，他們必須同時小於兩種型別，且程式沒有最大值檢查
```c
const Integer2byte MaxOfInteger2byte = INT_FAST16_MAX;
const unsigned char BitOfUinteger5Byte = (sizeof(Uinteger5byte) / sizeof(unsigned char)) * 8;
```
- `MaxOfInteger2byte`  Integer2byte 的最大值，如果 `Integer2byte` 不使用預設的 typedef 需要修改定義
- `BitOfUinteger5Byte` `Uinteger5byte` 的長度(Bit 不是 byte), 如果 `Uinteger5byte` 不使用預設的 typedef 需要修改定義
SRTINGLITERAL 要求要用 1 填充 Pading 到 Octet 邊緣
#### HpackStatus
Hpack 回傳的狀態
- `HpackStatus::TableSizeOverflow`
> 表達 DynamicTableSizeUpdate 要求的整數超過 `Hpack::SETTINGS_HEADER_TABLE_SIZE` 
#### IndexTable
管理靜態表及動態表，底層動態表是 vector，在擴容後不會自動減小。

#### 違背標準
1. StringLiteral 解碼時標準要求最末填充只能是 1 和填充不可以超過 7 bit 
        LiteralPadNotEOSMsb,
        LiteralPadLongThan7Bit,
LiteralDecodeCheckLiteralPad 關閉時沒做
2. 表上會有多個相同的 key 但 value 不同，標準沒說如只有 key 相同的情況下， index要選哪個，但標準的範例中選 index 最小值，我也跟著
StaticTable has priority in search. If two tables have the same key, StaticTable will be used.
3. DefaultStringLiteralEncode 設為 EShortest 時
4. HpackDecoderTableUpdateHeaderBlank 設為真
5. Header Field Representation 中字串長度及 Indexing Tables 的 index 超過實施的 size_t 和 Uinteger5byte 

valgrind --tool=memcheck ./benchmark

#### 引用聲明
- bench/Lucky_Kids.mp3
> "[11 - Lucky Kids - Gemafreie Klaviermusik - Ronny Matthes](https://www.jamendo.com/track/1225161)" by [Ronny Matthes - Pianist und Komponist (GEMA-frei / Royalty Free)](https://www.jamendo.com/artist/348759/matthesmusic) is licensed under [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/?ref=openverse).
- bench/Superman_Kid.mp3
> "[Superman Kid](https://www.jamendo.com/track/1294742)" by [Fresh Body Shop](https://www.jamendo.com/artist/7063/freshbodyshop) is licensed under [CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/?ref=openverse).
- bench/p-Groups.mp4
> [p-Groups](https://www.youtube.com/watch?v=V5eLqObp60Y)by [Andrew Misseldine](https://www.youtube.com/@Misseldine) is licensed under 創用 CC 姓名標示授權 (允許再利用)
- bench/previous_examination.mp4
> [歷屆考題賞析(p值的觀念)](https://www.youtube.com/watch?v=vFI1fYbIreo)by [CUSTCourses](https://www.youtube.com/@CUSTCourses) is licensed under 創用 CC 姓名標示授權 (允許再利用)
#### Performance Tuning 
Performance tuning 在 `-O1` 的環境下測試
測試後發現 `-O, -O1, -O2` 並沒有具體的速度提升
測試環境 i5-10300H Windows10 連接充電器 Mingw
DefaultStringLiteralEncode = EShortest
##### Base Line On Git 337b986054a9f99674b581dd72cf0501b475f657 
`gcc -O1`
```
With Error
```
VM Ubuntu 22.04 VCPU 2 Memory 3GB
```
OctetAll run 250 time in 1.160000ms.
LargeKvp run 250 time in 516.680000ms.
bench/template_parameters.data run 250 time in 6.280000ms.
bench/Gophercon.data run 250 time in 232.480000ms.
bench/Hlp.data run 250 time in 433.440000ms.
bench/Cppvec.data run 250 time in 294.720000ms.
bench/content_type.data run 250 time in 522.560000ms.
bench/example_domains.data run 250 time in 1.160000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 245.440000ms.
bench/go_http.data run 250 time in 1812.960000ms.
bench/HttplightSitemap.data run 250 time in 180.680000ms.
bench/NginxSitemap.data run 250 time in 2125.600000ms.
bench/HaproxySitemap.data run 250 time in 1057.800000ms.

```
##### 重構 Hpack On Git 215a5f346b791ce28fd979ab906c4564487edf1e
`gcc -O1`
```
OctetAll run 250 time in 1.000000ms.
LargeKvp run 250 time in 618.200000ms.
bench/template_parameters.data run 250 time in 6.000000ms.
bench/Gophercon.data run 250 time in 254.320000ms.
bench/Hlp.data run 250 time in 469.440000ms.
bench/Cppvec.data run 250 time in 348.160000ms.
bench/content_type.data run 250 time in 572.800000ms.
bench/example_domains.data run 250 time in 2.000000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 268.880000ms.
bench/go_http.data run 250 time in 1978.560000ms.
bench/HttplightSitemap.data run 250 time in 225.840000ms.
bench/NginxSitemap.data run 250 time in 2450.040000ms.
bench/HaproxySitemap.data run 250 time in 1263.400000ms.
```
VM Ubuntu 22.04 VCPU 2 Memory 3GB
```
OctetAll run 250 time in 0.440000ms.  
LargeKvp run 250 time in 526.080000ms.  
bench/template_parameters.data run 250 time in 5.040000ms.  
bench/Gophercon.data run 250 time in 227.320000ms.  
bench/Hlp.data run 250 time in 423.640000ms.  
bench/Cppvec.data run 250 time in 285.280000ms.  
bench/content_type.data run 250 time in 508.400000ms.  
bench/example_domains.data run 250 time in 1.000000ms.  
bench/clang_command.data run 250 time in 1.000000ms.  
bench/cc_sa.data run 250 time in 236.280000ms.  
bench/go_http.data run 250 time in 1746.920000ms.  
bench/HttplightSitemap.data run 250 time in 176.320000ms.  
bench/NginxSitemap.data run 250 time in 2024.520000ms.  
bench/HaproxySitemap.data run 250 time in 1004.160000ms.
```
- 用 `template<Bool SLEF>` 移除執行期分支
- `Hpack::_DecoderLoop` 發現 headerfield, kvp 有在迴圈中複製，用參照及 clear 加速
#####  移除接口
```
OctetAll run 250 time in 265.666667ms.
LargeKvp run 250 time in 448.333333ms.
bench/template_parameters.data run 250 time in 5.333333ms.
bench/Gophercon.data run 250 time in 215.333333ms.
bench/Hlp.data run 250 time in 404.333333ms.
bench/Cppvec.data run 250 time in 304.333333ms.
bench/content_type.data run 250 time in 495.666667ms.
bench/example_domains.data run 250 time in 1.000000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 241.000000ms.
bench/go_http.data run 250 time in 1822.666667ms.
bench/HttplightSitemap.data run 250 time in 192.333333ms.
bench/NginxSitemap.data run 250 time in 2135.000000ms.
bench/HaproxySitemap.data run 250 time in 1048.333333ms.
```
- 幾乎沒有影響

##### Prefix Tree Search by Switch
`gcc -O1`
```
EmptyKeyValPair run 250 time in 1.000000ms.
OctetAll run 250 time in 372.880000ms.
LargeKvp run 250 time in 252.400000ms.
bench/template_parameters.data run 250 time in 6.120000ms.
bench/Gophercon.data run 250 time in 181.920000ms.
bench/Hlp.data run 250 time in 359.280000ms.
bench/Cppvec.data run 250 time in 308.440000ms.
bench/content_type.data run 250 time in 424.000000ms.
bench/example_domains.data run 250 time in 1.560000ms.
bench/clang_command.data run 250 time in 1.080000ms.
bench/cc_sa.data run 250 time in 211.400000ms.
bench/go_http.data run 250 time in 1606.760000ms.
bench/HttplightSitemap.data run 250 time in 212.000000ms.
bench/NginxSitemap.data run 250 time in 2108.400000ms.
bench/HaproxySitemap.data run 250 time in 1133.080000ms.
```
```cpp
unsigned char __HuffmanSwitchffffffc00000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffffc00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)127;
        break;
    case (0xffffffd00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)220;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}
```
- 平均上有進步，但不多
- 發現 GCC 13 沒有查表
##### Switch Table
```
EmptyKeyValPair run 250 time in 1.000000ms.
OctetAll run 250 time in 380.200000ms.
LargeKvp run 250 time in 339.200000ms.
bench/template_parameters.data run 250 time in 5.000000ms.
bench/Gophercon.data run 250 time in 170.800000ms.
bench/Hlp.data run 250 time in 325.000000ms.
bench/Cppvec.data run 250 time in 283.600000ms.
bench/content_type.data run 250 time in 395.600000ms.
bench/example_domains.data run 250 time in 1.000000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 196.200000ms.
bench/go_http.data run 250 time in 1458.600000ms.
bench/HttplightSitemap.data run 250 time in 181.800000ms.
bench/NginxSitemap.data run 250 time in 2010.000000ms.
bench/HaproxySitemap.data run 250 time in 1103.000000ms.
```

##### add Hpack Api
`gcc -O1`
```
EmptyKeyValPair run 250 time in 0.000000ms.
OctetAll run 250 time in 377.800000ms.
LargeKvp run 250 time in 309.400000ms.
bench/template_parameters.data run 250 time in 6.000000ms.
bench/Gophercon.data run 250 time in 176.600000ms.
bench/Hlp.data run 250 time in 344.400000ms.
bench/Cppvec.data run 250 time in 300.000000ms.
bench/content_type.data run 250 time in 412.200000ms.
bench/example_domains.data run 250 time in 1.400000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 202.000000ms.
bench/go_http.data run 250 time in 1532.600000ms.
bench/HttplightSitemap.data run 250 time in 189.200000ms.
bench/NginxSitemap.data run 250 time in 2113.400000ms.
bench/HaproxySitemap.data run 250 time in 1131.800000ms.
```

##### Virtual Hpack::HpWBuffer On Git f11304a43700dfd67d0fec61241ba948eb39f2e7
`gcc -O1`
```
EmptyKeyValPair run 250 time in 1.000000ms.
OctetAll run 250 time in 393.000000ms.
LargeKvp run 250 time in 318.000000ms.
bench/template_parameters.data run 250 time in 6.000000ms.
bench/Gophercon.data run 250 time in 194.000000ms.
bench/Hlp.data run 250 time in 359.000000ms.
bench/Cppvec.data run 250 time in 310.000000ms.
bench/content_type.data run 250 time in 422.000000ms.
bench/example_domains.data run 250 time in 2.000000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 212.000000ms.
bench/go_http.data run 250 time in 1630.000000ms.
bench/HttplightSitemap.data run 250 time in 190.000000ms.
bench/NginxSitemap.data run 250 time in 2107.000000ms.
bench/HaproxySitemap.data run 250 time in 1150.000000ms.
```
- 變慢很多，好像只是沒插電效能比較遭

##### SwutchArray
`gcc -O1`
```
EmptyKeyValPair run 250 time in 1.000000ms.
OctetAll run 250 time in 328.800000ms.
LargeKvp run 250 time in 173.400000ms.
bench/template_parameters.data run 250 time in 6.000000ms.
bench/Gophercon.data run 250 time in 104.600000ms.
bench/Hlp.data run 250 time in 213.600000ms.
bench/Cppvec.data run 250 time in 258.200000ms.
bench/content_type.data run 250 time in 253.400000ms.
bench/example_domains.data run 250 time in 3.000000ms.
bench/clang_command.data run 250 time in 1.400000ms.
bench/cc_sa.data run 250 time in 140.000000ms.
bench/go_http.data run 250 time in 1097.000000ms.
bench/HttplightSitemap.data run 250 time in 156.600000ms.
bench/NginxSitemap.data run 250 time in 1725.800000ms.
bench/HaproxySitemap.data run 250 time in 984.000000ms.
```
- 還是回去查表
##### p

- change HuffmanCode lsb to msb ，減少 StringLiteralEncodeHuffman 中的計算量，將  HuffmanCode 數值推到最左側的工作交給建構試(應為需要知道 Uinteger5byte 的長度，所以不可以再 CodeGen.py 計算)運行
- Use unordered_map 據 [Ordered map vs. Unordered map – A Performance Study](http://supercomputingblog.com/windows/ordered-map-vs-unordered-map-a-performance-study/ "Permanent Link to Ordered map vs. Unordered map – A Performance Study") 表示 unordered_map 任何情況下接比 map 來的好
```bash
g++ -fno-align-functions -fno-align-loops --std=c++11 -O1 -o benchmark.exe benchmark.cpp hpack.cpp
g++ -fno-align-functions -fno-align-loops --std=c++11 -O2 -o benchmark.exe benchmark.cpp hpack.cpp
```
發現 -o2 慢 -o1 至少 20% 但 -o3 會恢復(快 -o1 1 到 2 %)
收尋後發現在 X86 機器上不同 CPU 會有不同的函式對其長度，但 GCC 預設建立對全部 X86 有效的機器碼，[這可能引響個別 CPU 的效率](https://stackoverflow.com/questions/19470873/why-does-gcc-generate-15-20-faster-code-if-i-optimize-for-size-instead-of-speed)，在實驗`-fno-align-functions -fno-align-loops` 對其無效後暫時放棄此猜測
在 ubuntu 22 機器上複測，異常消失，先暫時懷疑是 mingw-32 的問題

### p

- 動態表改成用 CircularBuffer
- 重構 StringLiteralDecodeHuffman