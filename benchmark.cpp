#define DEBUGFLAG 1
#include "hpack.h"

#include <array>
#include <string>
#include <vector>
#include <chrono>
#include <memory>
#include <fstream>
#include <algorithm>
#include <functional>

#include <cmath>

#if DEBUGFLAG == 1
#define RESULTCHECK 1
#endif // DEBUGFLAG

const bool BenchmarkFormatCsv = true;
const bool MedianAbsoluteDeviation = true;
const size_t DefaultHeaderCapacity = 1 << 13;
const size_t DefaultBufferCapacity = 1 << 13;
const unsigned long long DeEnBaseLoopTime = 10;
const unsigned long long BenchmarkLoopTime = 40;

typedef std::vector<std::pair<std::string,std::string> > HeaderVec;

struct HeaderVecPair {
    HeaderVec req, resp;
    HeaderVecPair():req(), resp() {
        ;
    }
    HeaderVecPair(const HeaderVec &q,const HeaderVec &p):req(q), resp(p) {
        ;
    }
};

struct FuncNameFunc {
  public:
    const char *funcName;
    std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> func;
    FuncNameFunc(const char *funcname, std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> f):funcName(funcname), func(f) {
        ;
    }

};

struct SubFlag {
  public:
    static const size_t HFDIGenMax = 7;
    static const unsigned char DecodeTyPtr = 1, DecodeTyHFDIPtr = 2,DecodeTyRef = 3, DecodeTyHFDIRef = 4;
    static const unsigned char EncodeTyPtr = 1, EncodeTyExcludePtr = 2, EncodeTyHFDIPtr = 3;
    static const unsigned char EncodeTyRef = 4, EncodeTyExcludeRef = 5, EncodeTyHFDIRef = 6;
    static const unsigned char EncodeTyWay = 7, EncodeTyExcludeWay = 8, EncodeTyHFDIWay = 9;
    static const unsigned char InputVec = 1, InputMem = 2;
    static const unsigned char HFDIAlwaysIndex = 1,HFDIIncrementalIndexing = 2, HFDINeverIndexed = 3, HFDIWithoutIndexing = 4, HFDIPlan0 = 5, HFDIPlan1 = 6, HFDIPlan2 = 7;

    size_t statusLstPosition = 0;
    unsigned char  encodeTy, decodeTy, encodeInput, docodeInput, hFDIGen;
    StringLiteralEncodeWay encodeWay;
    std::vector<std::string> exclude;
    std::vector<std::array<unsigned char, 5> > statusLst;
    SubFlag():encodeTy(1), decodeTy(1), encodeInput(1), docodeInput(1), hFDIGen(1), exclude(), statusLst() {
        ;
    }
    void Print() const {
        const char *decty[4] = {"Ptr", "HFDIPtr", "Ref", "HFDIRef"};
        const char *encty[9] = {"Ptr", "ExcludePtr", "HFDIPtr", "Ref", "ExcludeRef", "HFDIRef", "Way", "ExcludeWay", "HFDIWay"};
        const char *inputstr[2] = {"Vec", "Mem"};
        const char *hfdistr[HFDIGenMax] = {"AlwaysIndex","IncrementalIndexing","NeverIndexed","WithoutIndexing", "HFDIPlan0", "HFDIPlan1","HFDIPlan2"};
        printf("Input%s EncodeTy%s Input%s DecodeTy%s\n",inputstr[encodeInput-1],encty[encodeTy-1],inputstr[docodeInput-1], decty[decodeTy-1]);
        if(IsEncodeHFDI()) {
            printf("Encode HFDI GEN %s\n", hfdistr[hFDIGen-1]);
        }
        if(IsEncodeExclude()) {
            printf("Exclude:");
            for(const std::string &i : exclude) {
                printf("%s,", i.c_str());
            }
            printf("\n");
        }
    }
    void StatusListGen() {
        // Order encodeTy, decodeTy, encodeInput, docodeInput, hFDIGen
        bool stop = false;
        std::array<unsigned char, 5> lstmax = {10,5,2,3,2}, lst, _lst;
        const std::array<unsigned char, 5> initlst = {1,1,1,1,1};

        statusLst.clear();
        lst = initlst;
        while(!stop) {
            statusLst.push_back(lst);
            if(_IsEncodeHFDI(lst[0])) {
                //hFDIGen only use in EncodeHFDI
                _lst = lst;
                for(size_t i = 2 ; i < HFDIGenMax + 1; ++i) {
                    _lst[4] = i;
                    statusLst.push_back(_lst);
                }
            }
            lst[0] += 1;
            if(lst.at(0) == lstmax.at(0)) {
                lst[0] = initlst.at(0);
                for(size_t i = 1; i < lstmax.size(); ++i) {
                    if((++lst[i]) < lstmax.at(i)) {
                        break;
                    } else {
                        if(i == lstmax.size() - 1) {
                            stop = true;
                            break;
                        }
                        lst[i] = initlst.at(i);
                    }
                }
            }
        }
        return;
    }
    void StatusSet(const size_t pos) {
        // Order encodeTy, decodeTy, encodeInput, docodeInput, hFDIGen
        encodeTy = statusLst.at(pos).at(0);
        decodeTy = statusLst.at(pos).at(1);
        encodeInput= statusLst.at(pos).at(2);
        docodeInput  = statusLst.at(pos).at(3);
        hFDIGen = statusLst.at(pos).at(4);
        return;
    }
    bool StatusNext() {
        if(++statusLstPosition < statusLst.size()) {
            StatusSet(statusLstPosition);
            return true;
        }
        statusLstPosition = 0;
        return false;
    }

    static inline bool _IsEncodeHFDI(unsigned char _encodeTy) {
        return _encodeTy % 3 == 0;
    }

    bool IsEncodeWay()const {
        return encodeTy >= 7 && encodeTy <= 9;
    }
    bool IsEncodeExclude()const {
        return encodeTy % 3 == 2;
    }

    bool IsEncodeHFDI()const {
        return _IsEncodeHFDI(encodeTy);
    }
    bool IsDecodeHFDI()const {
        return decodeTy % 2 == 0;
    }
};

struct InputMemory {
    const size_t sizeByte;
    unsigned char *start;
    InputMemory(const std::vector<unsigned char> &ss):sizeByte(ss.size()), start(nullptr) {
        start = new unsigned char[sizeByte];
        if(start == nullptr) {
            printf("InputMemory::Constructor can not new memory.\n");
        }
        for(size_t i = 0; i < sizeByte; ++i) {
            *(start + i) = ss.at(i);
        }
    }
    ~InputMemory() {
        delete [] start;
    }
};

struct Boob {
    static const size_t buffMax = 3072;
    FILE *fp;
    const char *fileName;
    unsigned char buff[buffMax];
    Boob(const char *filename):fp(nullptr), fileName(filename), buff() {
        fp = fopen(filename, "rb");
        if(fp == nullptr) {
            printf("Boob::Constructor can not open %s.\n", filename);
            fflush(stdout);
        }
    }
    ~Boob() {
        fclose(fp);
    }
    bool ReadnByte(size_t n) {
        if( fread(buff, n, 1, fp) != 1) {
            if(ferror(fp) != 0) {
                printf("Boob::ReadnByte %s Ferror set.\n", fileName);
            }
            return false;
        }
        return true;
    }
    bool KeyAsciiValRandom(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
        size_t cn = 0,wb;
        KeyValPair kvp;
        reqheader.clear();
        resphrader.clear();
        while(cn < buffMax) {
            wb = (cn % 25) + 4;
            if(ReadnByte(wb)) {
                cn += wb;
                kvp.first.append((cn % 24) + 1, 'a' + (cn % 26));
                cn = cn + (cn % 24) + 1;
                for(size_t i = 0; i< wb; ++i) {
                    kvp.second.push_back(buff[i]);
                }
                reqheader.push_back(kvp);
                resphrader.push_back(kvp);
            } else {
                return false;
            }
            kvp.first.clear();
            kvp.second.clear();
        }
        return true;
    }
    bool KeyRandomValRandom(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
        size_t cn = 0,wb;
        std::string key, val;
        reqheader.clear();
        resphrader.clear();
        while(cn < buffMax) {
            wb = (cn % 120) + 4;
            if(ReadnByte(wb * 2)) {
                cn += wb * 2;
                for(size_t i = 0; i< wb; ++i) {
                    key.push_back(buff[i]);
                }
                for(size_t i = wb; i < wb*2; ++i) {
                    val.push_back(buff[i]);
                }
                reqheader.push_back(std::make_pair<std::string,std::string>(key.c_str(), val.c_str()));
                resphrader.push_back(std::make_pair<std::string,std::string>(key.c_str(), val.c_str()));
            } else {
                return false;
            }
            key.clear();
            val.clear();
        }
        return true;
    }
    bool HpRBufferRandom(std::vector<unsigned char> &vecbuff, size_t sizebyte = buffMax - 1) {
        if(ReadnByte(sizebyte - 1)) {
            for(size_t i = 0; i < sizebyte; ++i) {
                vecbuff.push_back(buff[i]);
            }
        } else {
            return false;
        }
        return true;
    }
};

struct WrapLoadFucntion {
    static const unsigned char UseFunc = 0;
    static const unsigned char UseHlst = 1;
    static const unsigned char UseBoobOct = 2;
    static const unsigned char UseBoobAscii = 3;

    const unsigned char use;
    std::unique_ptr<Boob> boob;
    std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hlst;
    std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> func;
    WrapLoadFucntion(const char *filename, bool oct):use(oct ? UseBoobOct : UseBoobAscii), boob(new Boob(filename)),hlst(nullptr),func() {
        ;
    }
    WrapLoadFucntion(std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > _hlst):use(UseHlst), boob(nullptr),hlst(std::move(_hlst)),func() {
        ;
    }
    WrapLoadFucntion(std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> loadfunc):use(UseFunc), boob(nullptr),hlst(nullptr),func(loadfunc) {
        ;
    }
    ~WrapLoadFucntion() = default;

    bool Load(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
        switch(use) {
        case UseFunc:
            return func(reqheader, resphrader, stage);
        case UseHlst:
            if((size_t)stage < hlst->size()) {
                reqheader.assign(hlst->at(stage)->req.begin(),hlst->at(stage)->req.end());
                resphrader.assign(hlst->at(stage)->resp.begin(), hlst->at(stage)->resp.end());
                return true;
            } else {
                return false;
            }
        case UseBoobOct:
            return boob->KeyRandomValRandom(reqheader, resphrader, stage);
        case UseBoobAscii:
            return boob->KeyAsciiValRandom(reqheader, resphrader, stage);
        default:
            printf("WrapLoadFucntion::Load use not allow.\n");
            return false;
        }
    }
    static std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> Bind(WrapLoadFucntion *self) {
        if(self == nullptr) {
            printf("WrapLoadFucntion::Bind receive nullptr self.\n");
        }
        std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)> f = std::bind(&WrapLoadFucntion::Load, self, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3);
        return f;
    }
};
std::string String2Hex(const std::string &s) {
    std::string r;
    const unsigned char trtable[17] = "0123456789abcdef";
    for(const unsigned char c : s) {
        r.append(1, trtable[(c & 0xf0) >> 4]);
        r.append(1, trtable[c & 0x0f]);
    }
    return r;
}

bool OctetAll(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool isend = false;
    KeyValPair kvp;
    std::string tmpstr;

    switch(stage) {
    case 0:
        for(size_t i = 0; i<256; ++i) {
            tmpstr.append(1, (unsigned char)i);
            if((i+1) % 16 == 0) {
                kvp.first.assign(tmpstr.begin(), tmpstr.begin() + (tmpstr.size()/2));
                kvp.second.assign(tmpstr.begin() + (tmpstr.size()/2),tmpstr.end());
                reqheader.push_back(kvp);
                resphrader.push_back(kvp);
                tmpstr.clear();
                kvp.first.clear();
                kvp.second.clear();
            }
        }
        break;
    case 1:
        for(size_t i = 0; i<256; ++i) {
            tmpstr.assign(8, (unsigned char)i);
            kvp.first.assign(tmpstr.begin(),tmpstr.end());
            kvp.second.assign(tmpstr.begin(),tmpstr.end());
            reqheader.push_back(kvp);
            resphrader.push_back(kvp);
        }
        break;
    default:
        isend = true;
        break;
    }
    return isend;
}

bool LargeKvp(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool isend = false;
    KeyValPair kvp;

    switch(stage) {
    case 0:
    case 1:
    case 2:
        for(size_t i = 0; i< 1024 * ((size_t)stage + 1); ++i) {
            kvp.first.append(1, (unsigned char)((i % 94) + 32));
            kvp.second.append(1, (unsigned char)((i % 94) + 32));
        }
        for(size_t i = 0; i<2; ++i) {
            reqheader.push_back(kvp);
            resphrader.push_back(kvp);
        }
        break;
    default:
        isend = true;
        break;
    }
    return isend;
}

bool EmptyKeyValPair(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool isend = false;

    switch(stage) {
    case 0:
        reqheader.push_back(std::make_pair<std::string, std::string>("accept-charset",""));
        reqheader.push_back(std::make_pair<std::string, std::string>("accept-encoding",""));
        reqheader.push_back(std::make_pair<std::string, std::string>(":method",""));
        reqheader.push_back(std::make_pair<std::string, std::string>(":status",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("accept-charset",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("accept-encoding",""));
        resphrader.push_back(std::make_pair<std::string, std::string>(":method",""));
        resphrader.push_back(std::make_pair<std::string, std::string>(":status",""));
        break;
    case 1:
        //return true;
        reqheader.push_back(std::make_pair<std::string, std::string>("",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("",""));
        break;
    case 2:
        for(size_t i =0; i< 12; ++i) {
            reqheader.push_back(std::make_pair<std::string, std::string>("",""));
            resphrader.push_back(std::make_pair<std::string, std::string>("",""));
        }
        break;
    case 3:
        reqheader.push_back(std::make_pair<std::string, std::string>("","http"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","https"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","206"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","XYZ"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","http"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","https"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","206"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","XYZ"));
        break;
    /*
    case 4:
        break;
    case 5:
        break;
    */
    default:
        isend = true;
        break;
    }
    return isend;
}

const std::array<const char*, 4> LoadBoobLst = {"bench/Lucky_Kids.mp3","bench/Superman_Kid.mp3" ,"bench/p-Groups.mp4", "bench/previous_examination.mp4"};
const std::array<FuncNameFunc, 3> LoadFuncLst = {FuncNameFunc("EmptyKeyValPair",EmptyKeyValPair),FuncNameFunc("OctetAll",OctetAll),FuncNameFunc("LargeKvp",LargeKvp)};
const std::array<const char*, 12> BenchDataFileName = {"bench/template_parameters.data","bench/Gophercon.data","bench/Hlp.data","bench/Cppvec.data","bench/content_type.data","bench/example_domains.data","bench/clang_command.data","bench/cc_sa.data","bench/go_http.data","bench/HttplightSitemap.data","bench/NginxSitemap.data","bench/HaproxySitemap.data"};

void PrintVecHeaderVecPair(std::vector<std::shared_ptr<HeaderVecPair> > hlst, bool ashex = false) {
    for(auto i : hlst) {
        printf("Request\n");
        for(auto j : i->req) {
            if(ashex) {
                printf("%s : %s\n", String2Hex(j.first).c_str(), String2Hex(j.second).c_str());
            } else {
                printf("%s : %s\n", j.first.c_str(), j.second.c_str());
            }
        }
        printf("Respones\n");
        for(auto j : i->resp) {
            if(ashex) {
                printf("%s : %s\n", String2Hex(j.first).c_str(), String2Hex(j.second).c_str());
            } else {
                printf("%s : %s\n", j.first.c_str(), j.second.c_str());
            }
        }
    }
    return;
}

inline const char* PrintHeaderFieldRepresentation(HeaderFieldRepresentation hfrp) {
    switch(hfrp) {
    case RInitial:
        return "Initial";
    case RIndexedHeaderField:
        return "Indexed";
    case RLiteralHeaderFieldWithIncrementalIndexing:
        return "WithIncrementalIndexing";
    case RDynamicTableSizeUpdate:
        return "DynamicTableSizeUpdate";
    case RLiteralHeaderFieldNeverIndexed:
        return "NeverIndexed";
    case RLiteralHeaderFieldWithoutIndexing:
        return "WithoutIndexing";
    case RLiteralHeaderFieldAlwaysIndex:
        return "AlwaysIndex";
    default:
        return "PrintHeaderFieldRepresentationDefault";
    }
}

void PrintHfdiList(const std::vector<HeaderFieldInfo> &hfdilst) {
    for(const HeaderFieldInfo &hfdi : hfdilst) {
        printf("Repr %s evictCounter %lu.\n", PrintHeaderFieldRepresentation(hfdi.representation), hfdi.evictCounter);
    }
}

bool ParserKeyVal(std::string &line,const size_t linecn,HeaderVec &vec) {
    bool escape, exist;
    size_t poscn;
    std::string keystr,valuestr,*wstr;
    unsigned char stat;

    stat = 0;
    poscn = 0;
    exist = false;
    line += " ";
    wstr = &keystr;
    escape = false;
    for(char c : line) {
        switch(c) {
        case ' ':
            if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '\\');
                }
                wstr->append(1, ' ');
            } else {
                ;
            }
            break;
        case '"':
            if(stat == 0 || stat == 2) {
                stat += 1;
                if(stat == 3) {
                    wstr = &valuestr;
                }
            } else if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '"');
                } else {
                    stat += 1;
                    if(stat == 4) {
                        exist = true;
                    }
                }
            }
            break;
        case '\\':
            if(stat == 1 || stat == 3) {
                escape = true;
            } else {
                exist = true;
                printf("Line %lu Pos %lu Unexpect \\.\n", linecn, poscn);
            }
            break;
        default:
            if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '\\');
                }
                wstr->append(1, c);
            } else {
                exist = true;
                printf("Line %lu Pos %lu Unexpect '%c'.\n", linecn, poscn, c);
            }
            break;
        }
        if(exist) {
            break;
        }
        ++poscn;
    }
    if(stat == 4) {
        vec.push_back(std::make_pair<std::string,std::string>(keystr.c_str(), valuestr.c_str()));
        return true;
    } else {
        printf("Line %lu stop at Unexpect status(%u).\n", linecn, stat);
        return false;
    }
}

bool LoadBenchmarkData(std::string filename, std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    bool exit, readfn;
    size_t linecn;
    char funcname[256];
    HeaderVec *vec;
    std::string line;
    std::ifstream fd;
    std::shared_ptr<HeaderVecPair> hvp;

    fd.open(filename, std::ios::in);
    if(!fd.is_open()) {
        printf("Can not open %s.\n", filename.c_str());
    }

    vec = nullptr;
    exit = false;
    linecn = 0;
    readfn = false;
    while(!fd.eof()) {
        ++linecn;
        std::getline(fd, line);
        if(line.size() == 0) {
            continue;
        }
        switch(line.at(0)) {
        case '#':
            break;
        case 'F':
            if(! (sscanf(line.c_str(), "FunctionName %255s", funcname) == 1)) {
                exit = true;
                printf("Line %lu not follow \"FunctionName %%255s\" format(%s).\n", linecn, line.c_str());
            }
            if(readfn) {
                exit = true;
                printf("Line %lu only allow one function in file.\n", linecn);
            }
            vec = nullptr;
            readfn = true;
            break;
        case 'Q':
            if(hvp) {
                hlst.push_back(hvp);
            }
            hvp = std::make_shared<HeaderVecPair>();
            vec = &(hvp->req);
            break;
        case 'P':
            if(!hvp) {
                exit = true;
                printf("Line %lu Use \"FunctionName %%255s\" before P.\n", linecn);
            }
            vec = &(hvp->resp);
            break;
        case '"':
            if(vec == nullptr) {
                exit = true;
                printf("Line %lu Use Q or P before \"%%8191s\"\"%%8191s\".\n", linecn);
            }
            if(!ParserKeyVal(line, linecn, *vec)) {
                printf("Line %lu not follow \"%%8191s\"\"%%8191s\" format(%s).\n", linecn, line.c_str());
            }
            break;
        default:
            printf("Line %lu Unknow(%s).\n", linecn, line.c_str());
        }
        if(exit) {
            break;
        }
    }
    bool finishSuccess = true;
    if(!fd.eof()) {
        finishSuccess = false;
    }
    fd.close();
    return finishSuccess;
}

void PerpareHeader(std::vector<std::shared_ptr<HeaderVecPair> > &hlst, std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> funcptr) {
    int stage;
    HeaderVec qheader, pheader;
    std::shared_ptr<HeaderVecPair> hvp;

    stage = 0;
    hvp = std::make_shared<HeaderVecPair>();
    while(!funcptr(hvp->req,hvp->resp, stage++)) {
        hlst.push_back(hvp);
        hvp = std::make_shared<HeaderVecPair>();
    }
    return;
}

void RandomItem() {
    Hpack hpack;
    HeaderVec header;
    std::unique_ptr<Boob> boob;
    std::vector<unsigned char> buff;
    for(const char * filename : LoadBoobLst) {
        boob.reset(new Boob(filename));
        boob->HpRBufferRandom(buff);
        hpack.Decoder(Hpack::MakeHpRBuffer(buff), header);
        buff.clear();
        header.clear();
        printf("%s RandomItem.\n", filename);
    }
    return;
}

bool _LoadItem(const int testDataSet, const bool rthlst,std::string &funcname, int stage, std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hlst, std::unique_ptr<WrapLoadFucntion> &wrapfunc) {
    if(testDataSet == 0) {
        return false;
    }
    if((size_t)stage < LoadFuncLst.size()) {
        funcname.assign(LoadFuncLst.at(stage).funcName);
        if(rthlst) {
            PerpareHeader(*(hlst.get()), LoadFuncLst.at(stage).func);
        } else {
            wrapfunc.reset(new WrapLoadFucntion(LoadFuncLst.at(stage).func));
        }
        return true;
    }
    if(testDataSet <= 1) {
        return false;
    }
    stage = stage - LoadFuncLst.size();
    if((size_t)stage < BenchDataFileName.size()) {
        funcname.assign(BenchDataFileName.at(stage));
        LoadBenchmarkData(BenchDataFileName.at(stage),*(hlst.get()));
        if(!rthlst) {
            wrapfunc.reset(new WrapLoadFucntion(hlst));
        }
        return true;
    }
    if(testDataSet <= 2) {
        return false;
    }
    stage = stage - BenchDataFileName.size();
    if((size_t)stage < LoadBoobLst.size()) {
        funcname.assign(LoadBoobLst.at(stage));
        if(rthlst) {
            Boob boob(LoadBoobLst.at(stage));
            PerpareHeader(*(hlst.get()), std::bind(&Boob::KeyRandomValRandom, &boob, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3));
        } else {
            wrapfunc.reset(new WrapLoadFucntion(LoadBoobLst.at(stage), true));
        }
        return true;
    }
    if(testDataSet <= 3) {
        return false;
    }
    stage = stage - BenchDataFileName.size();
    if((size_t)stage < LoadBoobLst.size()) {
        funcname.assign(LoadBoobLst.at(stage));
        if(rthlst) {
            Boob boob(LoadBoobLst.at(stage));
            PerpareHeader(*(hlst.get()), std::bind(&Boob::KeyAsciiValRandom, &boob, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3));
        } else {
            wrapfunc.reset(new WrapLoadFucntion(LoadBoobLst.at(stage), false));
        }
        return true;
    }
    return false;
}

bool LoadTestItem(std::string &funcname, int stage,std::unique_ptr<WrapLoadFucntion> &wrapfunc) {
    wrapfunc.reset(nullptr);
    return _LoadItem(4, false, funcname,stage, std::make_shared<std::vector<std::shared_ptr<HeaderVecPair> > >(), wrapfunc);
}

struct NullDeleterHack {
    template<class T>
    void operator()(T* p) {
        ;
    }
};

bool LoadBenchmarkItem(std::string &funcname, std::vector<std::shared_ptr<HeaderVecPair> > &hlst, int &stage) {
    std::unique_ptr<WrapLoadFucntion> wrapfunc;
    std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hptr(&hlst, NullDeleterHack {});
    return _LoadItem(2, true, funcname,stage, hptr, wrapfunc);
}

bool LoadDynamicTableUpdateItem(std::string &funcname, std::vector<std::shared_ptr<HeaderVecPair> > &hlst, int &stage) {
    std::unique_ptr<WrapLoadFucntion> wrapfunc;
    std::shared_ptr<std::vector<std::shared_ptr<HeaderVecPair> > > hptr(&hlst, NullDeleterHack {});
    return _LoadItem(4, true, funcname,stage, hptr, wrapfunc);
}

void DeEnFailCheck(const HeaderVec &f, const HeaderVec &b,const std::string &failat,HpackStatus enstat,HpackStatus destat) {
    if(enstat != HpackStatus::Success) {
        printf("Hpack Encode fail %s at %s.\n", Hpack::HpackStatus2String(enstat).c_str(), failat.c_str());
        return;
    }
    if(destat != HpackStatus::Success) {
        printf("Hpack Decode fail %s at %s.\n", Hpack::HpackStatus2String(destat).c_str(), failat.c_str());
        return;
    }
    if(f.size() != b.size()) {
        printf("Hpack DeEn header not same length En %lu De %lu  at %s.\n", f.size(), b.size(), failat.c_str());
        return;
    }
    if(f != b) {
        printf("Hpack DeEn %s Fail.\n", failat.c_str());
        for(size_t i = 0; i < f.size(); ++i) {
            if(f.at(i) != b.at(i)) {
                //KeyValPairPrint(f.at(i));
                //KeyValPairPrint(b.at(i));
                printf("%lu Fail\n", i);
                printf("%s : %s\n", String2Hex(f.at(i).first).c_str(), String2Hex(f.at(i).second).c_str());
                printf("%s : %s\n", String2Hex(b.at(i).first).c_str(), String2Hex(b.at(i).second).c_str());
            } else {
                printf("%lu Pass\n", i);
                printf("%s : %s\n", String2Hex(f.at(i).first).c_str(), String2Hex(f.at(i).second).c_str());
            }
        }
    }
    return;
}

void SubCheckClear(const HeaderVec &f,HeaderVec &headerclear,const std::string &failat,HpackStatus enstat,HpackStatus destat,const Hpack &fhpa,const Hpack &bhpa, const SubFlag &subflag, std::vector<HeaderFieldInfo> &ehfdi,std::vector<HeaderFieldInfo> &dhfdi) {
#if RESULTCHECK == 1
    DeEnFailCheck(f, headerclear, failat, enstat, destat);
    if(fhpa.DynamicTableMaxSize() != bhpa.DynamicTableMaxSize()) {
        printf("Hpack DynamicTableMaxSize not same En %lu De %lu at %s.\n", fhpa.DynamicTableMaxSize(), bhpa.DynamicTableMaxSize(), failat.c_str());
    }
    if(fhpa.DynamicTableSize() != bhpa.DynamicTableSize()) {
        printf("Hpack DynamicTableSize not same En %lu De %lu at %s.\n", fhpa.DynamicTableSize(), bhpa.DynamicTableSize(), failat.c_str());
    }
    if(subflag.IsEncodeHFDI()&& subflag.IsDecodeHFDI()) {
        if(ehfdi.size() != dhfdi.size()) {
            printf("Hpack HFDIlst not same length En %lu De %lu at %s.\n", ehfdi.size(), dhfdi.size(), failat.c_str());
        }
        if(ehfdi != dhfdi) {
            printf("Hpack HFDIlst %s fail.\n", failat.c_str());
            for(size_t i = 0; i < ehfdi.size(); ++i) {
                if(ehfdi.at(i) == dhfdi.at(i)) {
                    printf("%lu Pass\n", i);
                    printf("evictCounter %lu representation %s\n", ehfdi.at(i).evictCounter, PrintHeaderFieldRepresentation(ehfdi.at(i).representation));
                } else {
                    printf("%lu Fail\n", i);
                    printf("evictCounter %lu representation %s\n", ehfdi.at(i).evictCounter, PrintHeaderFieldRepresentation(ehfdi.at(i).representation));
                    printf("evictCounter %lu representation %s\n", dhfdi.at(i).evictCounter, PrintHeaderFieldRepresentation(dhfdi.at(i).representation));
                }
            }
        }

    }
#endif // RESULTCHECK
    ehfdi.clear();
    dhfdi.clear();
    headerclear.clear();
    return;
}

void DeEnSubHfdi(const SubFlag &subflag,std::vector<HeaderFieldInfo>& ehfdi, const HeaderVec &feed) {
    HeaderFieldInfo hfdi;
    static const std::array<HeaderFieldRepresentation, 2> plan0 = {HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed,HeaderFieldRepresentation::RLiteralHeaderFieldAlwaysIndex};
    static const std::array<HeaderFieldRepresentation, 3> plan1 = {HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed,HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing, HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed};
    static const std::array<HeaderFieldRepresentation, 4> plan2 = {HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing, HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing, HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed, HeaderFieldRepresentation::RLiteralHeaderFieldAlwaysIndex};
    for(size_t i = 0 ; i < feed.size(); ++i) {
        switch(subflag.hFDIGen) {
        case SubFlag::HFDIAlwaysIndex:
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldAlwaysIndex;
            break;
        case SubFlag::HFDIIncrementalIndexing:
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing;
            break;
        case SubFlag::HFDINeverIndexed:
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed;
            break;
        case SubFlag::HFDIWithoutIndexing:
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing;
            break;
        case SubFlag::HFDIPlan0:
            hfdi.representation = plan0.at((i % plan0.size()));
            break;
        case SubFlag::HFDIPlan1:
            hfdi.representation = plan1.at((i % plan1.size()));
            break;
        case SubFlag::HFDIPlan2:
            hfdi.representation = plan2.at((i % plan2.size()));
            break;
        default:
            printf("Unexpect SubFlag::hFDIGen(%u) on Encoder.\n",subflag.hFDIGen);
        }
        ehfdi.push_back(hfdi);
    }
    return;
}

void DeEnSub(std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int)>  loadlst,const SubFlag &subflag) {
    int loadstage;
    HeaderVec header;
    HpackStatus enstat, destat;
    Hpack reqs, resps,reqr, respr;
    HeaderVecPair hpair;
    std::vector<std::string> exclude;
    std::vector<unsigned char> buff;
    std::vector<HeaderFieldInfo> ehfdi, dhfdi;

    buff.reserve(DefaultBufferCapacity);
    header.reserve(DefaultHeaderCapacity);
    exclude.assign(subflag.exclude.begin(), subflag.exclude.end());

    auto _internal_sub = [&] (Hpack &send, Hpack &recv, HeaderVec &feed) mutable -> void {
        std::unique_ptr<HpRBuffer> decodeInput;
        std::shared_ptr<InputMemory> ipm;
        auto encodeInput = Hpack::MakeHpWBuffer(buff, 16384);

        if(subflag.encodeTy == SubFlag::EncodeTyPtr) {
            enstat = send.Encoder(std::move(encodeInput),feed);
        } else if(subflag.encodeTy == SubFlag::EncodeTyRef) {
            enstat = send.Encoder(*encodeInput,feed);
        } else if(subflag.encodeTy == SubFlag::EncodeTyWay) {
            enstat = send.Encoder(*encodeInput,feed);
        } else if(subflag.encodeTy == SubFlag::EncodeTyExcludePtr) {
            enstat = send.Encoder(std::move(encodeInput),feed, exclude);
        } else if(subflag.encodeTy == SubFlag::EncodeTyExcludeRef) {
            enstat = send.Encoder(*encodeInput,feed, exclude);
        } else if(subflag.encodeTy == SubFlag::EncodeTyExcludeWay) {
            enstat = send.Encoder(*encodeInput,feed, exclude);
        } else if(subflag.IsEncodeHFDI()) {
            DeEnSubHfdi(subflag, ehfdi, feed);
            enstat = send.Encoder(std::move(encodeInput),feed, ehfdi);
        }

        if(subflag.docodeInput == SubFlag::InputVec) {
            decodeInput = Hpack::MakeHpRBuffer(buff);
        } else if(subflag.docodeInput == SubFlag::InputMem) {
            ipm = std::make_shared<InputMemory>(buff);
            decodeInput = Hpack::MakeHpRBuffer(ipm->start, ipm->sizeByte);
        }

        switch(subflag.decodeTy) {
        case SubFlag::DecodeTyPtr:
            destat = recv.Decoder(std::move(decodeInput), header);
            break;
        case SubFlag::DecodeTyRef:
            destat = recv.Decoder(*decodeInput, header);
            break;
        case SubFlag::DecodeTyHFDIPtr:
            destat = recv.Decoder(std::move(decodeInput), header, dhfdi);
            break;
        case SubFlag::DecodeTyHFDIRef:
            destat = recv.Decoder(*decodeInput, header, dhfdi);
            break;
        }
    };

    for(int i = 0; i < 1; ++i) {
        loadstage = 0;
        while(loadlst(hpair.req, hpair.resp, loadstage++)) {
            _internal_sub(reqs, reqr, hpair.req);
            SubCheckClear(hpair.req, header, "Request", enstat,destat,reqs,reqr, subflag, ehfdi, dhfdi);
            buff.clear();

            _internal_sub(resps, respr, hpair.resp);
            SubCheckClear(hpair.resp, header, "Response", enstat,destat,resps,respr, subflag, ehfdi, dhfdi);
            buff.clear();
        }
    }
    return;
}

void TestItem() {
    int stage;
    bool endStage, statusNext;
    size_t arrpos;
    SubFlag subflag;
    std::string funcorfilename;
    std::unique_ptr<WrapLoadFucntion> wrapfunc;
    const std::array<StringLiteralEncodeWay, 3> literalEncodeWay = {StringLiteralEncodeWay::EOctet, StringLiteralEncodeWay::EHuffman, StringLiteralEncodeWay::EShortest};

    stage = 0;
    arrpos = 0;
    endStage = false;
    subflag.StatusListGen();
    while(stage < 50) {
        do {
            if(!LoadTestItem(funcorfilename,stage, wrapfunc)) {
                endStage = true;
                break;
            }
            if(subflag.IsEncodeExclude()) {
                subflag.exclude.assign({"authorization", "set-cookie", "cookie", "token"});
            }
            //printf("%s Test ", funcorfilename.c_str());
            //subflag.Print();

            DeEnSub(WrapLoadFucntion::Bind(wrapfunc.get()), subflag);

            if(subflag.IsEncodeWay()) {
                if(arrpos < literalEncodeWay.size()) {
                    statusNext = true;
                    subflag.encodeWay = literalEncodeWay.at(arrpos++);
                } else {
                    arrpos = 0;
                    statusNext = subflag.StatusNext();
                }
            } else {
                statusNext = subflag.StatusNext();
            }
        } while(statusNext);
        if(endStage) {
            break;
        }
        stage++;
        printf("%s run TestItem.\n", funcorfilename.c_str());
    }
    return;
}

void _DeEnDynamicTableUpdate(const HeaderVec &feed, Hpack &send, Hpack &recv, const std::string &failat, const size_t hlstpos) {
    HeaderVec header, h, f;
    const SubFlag subflag;
    HpackStatus enstat, destat;
    HeaderFieldInfo hfdi;
    std::vector<unsigned char> buffpre, buff;
    std::vector<HeaderFieldInfo> hfdilst;

    if(hlstpos % 7 == 0) {
        std::unique_ptr<HpWBuffer> wstream = std::move(Hpack::MakeHpWBuffer(buffpre));
        send.EncoderDynamicTableSizeUpdate(*(wstream.get()),(hlstpos % 11) * 150, hfdi);
    }
    enstat = send.Encoder(Hpack::MakeHpWBuffer(buff, 12288), feed);
    buffpre.insert(buffpre.end(), buff.begin(), buff.end());
    destat = recv.Decoder(Hpack::MakeHpRBuffer(buffpre), header);
    for(const std::pair<std::string,std::string> &kvp : feed) {
        if(kvp.first.size() != 0 || kvp.second.size() != 0) {
            f.push_back(kvp);
        }
    }
    for(const std::pair<std::string,std::string> &kvp : header) {
        if(kvp.first.size() != 0 || kvp.second.size() != 0) {
            h.push_back(kvp);
        }
    }
    SubCheckClear(f, h, failat, enstat, destat, send, recv, subflag, hfdilst, hfdilst);
}

void DeEnDynamicTableUpdate(const std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    size_t settingheadertablesize = 16384;
    Hpack reqs(&settingheadertablesize), resps(&settingheadertablesize),reqr(&settingheadertablesize), respr(&settingheadertablesize);
    for(size_t i = 0; i < hlst.size() ; ++i) {
        _DeEnDynamicTableUpdate(hlst.at(i)->req, reqs, reqr, "Request", i);
        _DeEnDynamicTableUpdate(hlst.at(i)->resp, resps, respr, "Response", i);
    }
    return;
}

void DynamicTableUpdateItem() {
    int stage;
    std::string funcorfilename;
    std::vector<std::shared_ptr<HeaderVecPair> > hlst;

    stage = 0;
    while(LoadDynamicTableUpdateItem(funcorfilename, hlst, stage) && stage < 50) {
        stage++;
        DeEnDynamicTableUpdate(hlst);
        printf("%s run DynamicTableUpdateItem.\n", funcorfilename.c_str());
    }

    return;
}

unsigned long DeEnBase(const std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    unsigned long durmicro;
    HeaderVec qheader, pheader;
    HpackStatus enstat, destat;
    Hpack reqs, resps,reqr, respr;
    std::vector<unsigned char> qbuff, pbuff;
    std::chrono::steady_clock::time_point start;
    qheader.reserve(DefaultHeaderCapacity);
    pheader.reserve(DefaultHeaderCapacity);
    qbuff.reserve(DefaultBufferCapacity);
    pbuff.reserve(DefaultBufferCapacity);
    start = std::chrono::steady_clock::now();
    for(unsigned long long i = 0; i < DeEnBaseLoopTime; ++i) {
        for(const std::shared_ptr<HeaderVecPair>  &hpair : hlst) {
            enstat = reqs.Encoder(Hpack::MakeHpWBuffer(qbuff, 12288),hpair->req);
            destat = reqr.Decoder(Hpack::MakeHpRBuffer(qbuff), qheader);
#if RESULTCHECK
            DeEnFailCheck(hpair->req, qheader, "Request", enstat,destat);
#endif // RESULTCHECK
            enstat = resps.Encoder(Hpack::MakeHpWBuffer(pbuff, 12288), hpair->resp);
            destat = respr.Decoder(Hpack::MakeHpRBuffer(pbuff), pheader);
#if RESULTCHECK
            DeEnFailCheck(hpair->resp, pheader, "Response",enstat,destat);
#endif // RESULTCHECK
            qbuff.clear();
            pbuff.clear();
            qheader.clear();
            pheader.clear();
        }
    }
    durmicro = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - start).count();
    return durmicro;
}

void BenchmarkItem() {
    int stage;
    double median, mad, average;
    unsigned long long madt;
    const unsigned long long truncateN = BenchmarkLoopTime / 10;
    std::string funcorfilename;
    std::array<double, BenchmarkLoopTime> microsecond;
    std::vector<std::shared_ptr<HeaderVecPair> > hlst;
    std::chrono::steady_clock::time_point start;

    stage = 0;
    if(BenchmarkFormatCsv){
        while(LoadBenchmarkItem(funcorfilename, hlst,stage) && stage < 50) {
            printf("%s,", funcorfilename.c_str());
            ++stage;
        }
        printf("\n");
    }

    stage = 0;
    while(LoadBenchmarkItem(funcorfilename, hlst,stage) && stage < 50) {
        stage++;
        for(unsigned long long i = 0; i < BenchmarkLoopTime; ++i) {
            microsecond[i] = DeEnBase(hlst);
        }
        std::sort(microsecond.begin(), microsecond.end());
        if(MedianAbsoluteDeviation) {
            median = microsecond.at(BenchmarkLoopTime / 2);
            if(BenchmarkFormatCsv){
                printf("%f,",median);
            }else{
                madt = 0;
                for(unsigned long long i = truncateN; i < BenchmarkLoopTime - truncateN; ++i) {
                    madt = madt + (unsigned long long)fabs(microsecond.at(i) - median);
                }
                mad = (double)madt / (double)(BenchmarkLoopTime - truncateN * 2);
                printf("%s run %llu time in Median %.0lf microsecond With Median absolute deviation %f(%f%%).\n", funcorfilename.c_str(),DeEnBaseLoopTime, median,mad, mad/median);
            }
        } else { //Average
            average = 0;
            for(unsigned long long i = truncateN; i < BenchmarkLoopTime - truncateN; ++i) {
                average = average + microsecond.at(i);
            }
            average = average / (double)(BenchmarkLoopTime - truncateN * 2);
            if(BenchmarkFormatCsv){
                printf("%f,",average);
            }else{
                printf("%s run %llu time Average %f microsecond.\n", funcorfilename.c_str(),DeEnBaseLoopTime,average);
            }
        }
        hlst.clear();
    }
    if(BenchmarkFormatCsv){
        printf("\n");
    }
}

int main() {
    //TestItem();
    //RandomItem();
    BenchmarkItem();
    //DynamicTableUpdateItem();
    return 0;
}
